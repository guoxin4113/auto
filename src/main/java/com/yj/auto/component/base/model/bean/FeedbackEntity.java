package com.yj.auto.component.base.model.bean;

import java.util.Date;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;
import com.yj.auto.core.jfinal.base.*;

/**
 *  系统反馈
 */
@SuppressWarnings("serial")
public abstract class FeedbackEntity<M extends FeedbackEntity<M>> extends BaseEntity<M> {
	
	public static final String TABLE_NAME = "t_com_feedback"; //数据表名称
	
	public static final String TABLE_PK = "id"; //数据表主键
	
	public static final String TABLE_REMARK = "系统反馈"; //数据表备注

	public String getTableName(){
		return TABLE_NAME;
	}

	public String getTableRemark(){
		return TABLE_REMARK;
	}
	
	public String getTablePK(){
		return TABLE_PK;
	}
	
   		
	/**
	 * Column ：id
	 * @return 主键
	 */
   		
	public Integer getId(){
   		return get("id");
   	}
	
	public void setId(Integer id){
   		set("id" , id);
   	}	
   		
	/**
	 * Column ：title
	 * @return 标题
	 */
   	@NotBlank
	@Length(max = 256)	
	public String getTitle(){
   		return get("title");
   	}
	
	public void setTitle(String title){
   		set("title" , title);
   	}	
   		
	/**
	 * Column ：type
	 * @return 类型
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getType(){
   		return get("type");
   	}
	
	public void setType(String type){
   		set("type" , type);
   	}	
   		
	/**
	 * Column ：src
	 * @return 来源
	 */
   	@Length(max = 64)	
	public String getSrc(){
   		return get("src");
   	}
	
	public void setSrc(String src){
   		set("src" , src);
   	}	
   		
	/**
	 * Column ：user_id
	 * @return 反馈人ID
	 */
   		
	public Integer getUserId(){
   		return get("user_id");
   	}
	
	public void setUserId(Integer userId){
   		set("user_id" , userId);
   	}	
   		
	/**
	 * Column ：user_name
	 * @return 反馈人名称
	 */
   	@NotBlank
	@Length(max = 64)	
	public String getUserName(){
   		return get("user_name");
   	}
	
	public void setUserName(String userName){
   		set("user_name" , userName);
   	}	
   		
	/**
	 * Column ：ctime
	 * @return 反馈日期
	 */
	public Date getCtime(){
   		return get("ctime");
   	}
	
	public void setCtime(Date ctime){
   		set("ctime" , ctime);
   	}	
   		
	/**
	 * Column ：content
	 * @return 反馈内容
	 */
   	@Length(max = 65535)	
	public String getContent(){
   		return get("content");
   	}
	
	public void setContent(String content){
   		set("content" , content);
   	}	
   		
	/**
	 * Column ：state
	 * @return 状态
	 */
   	@NotBlank
	@Length(max = 32)	
	public String getState(){
   		return get("state");
   	}
	
	public void setState(String state){
   		set("state" , state);
   	}	
   		
	/**
	 * Column ：param1
	 * @return 参数一
	 */
   	@Length(max = 512)	
	public String getParam1(){
   		return get("param1");
   	}
	
	public void setParam1(String param1){
   		set("param1" , param1);
   	}	
   		
	/**
	 * Column ：param2
	 * @return 参数二
	 */
   	@Length(max = 512)	
	public String getParam2(){
   		return get("param2");
   	}
	
	public void setParam2(String param2){
   		set("param2" , param2);
   	}	
   		
	/**
	 * Column ：param3
	 * @return 参数三
	 */
   	@Length(max = 512)	
	public String getParam3(){
   		return get("param3");
   	}
	
	public void setParam3(String param3){
   		set("param3" , param3);
   	}	
   		
	/**
	 * Column ：luser
	 * @return 最后编辑人
	 */
   		
	public Integer getLuser(){
   		return get("luser");
   	}
	
	public void setLuser(Integer luser){
   		set("luser" , luser);
   	}	
   		
	/**
	 * Column ：ltime
	 * @return 最后编辑时间
	 */
   		
	public Date getLtime(){
   		return get("ltime");
   	}
	
	public void setLtime(Date ltime){
   		set("ltime" , ltime);
   	}	
   		
	/**
	 * Column ：reply_id
	 * @return 回复人ID
	 */
   		
	public Integer getReplyId(){
   		return get("reply_id");
   	}
	
	public void setReplyId(Integer replyId){
   		set("reply_id" , replyId);
   	}	
   		
	/**
	 * Column ：reply_time
	 * @return 回复日期
	 */
   		
	public Date getReplyTime(){
   		return get("reply_time");
   	}
	
	public void setReplyTime(Date replyTime){
   		set("reply_time" , replyTime);
   	}	
   		
	/**
	 * Column ：reply_content
	 * @return 回复内容
	 */
   	@Length(max = 65535)	
	public String getReplyContent(){
   		return get("reply_content");
   	}
	
	public void setReplyContent(String replyContent){
   		set("reply_content" , replyContent);
   	}	
}