package com.yj.auto.core.jfinal.directive;


import javax.servlet.http.HttpSession;

import com.jfinal.kit.StrKit;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.yj.auto.core.base.online.SessionUser;
import com.yj.auto.core.base.online.SessionUserUtil;

public class AuthDirective extends Directive {

	private Expr uri;
	private Expr res;

	public void setExprList(ExprList exprList) {
		Expr[] arr = exprList.getExprArray();
		this.uri = arr[0];
		this.res = arr[1];
	}

	@Override
	public void exec(Env env, Scope scope, Writer writer) {
		HttpSession session = (HttpSession) scope.get("session");
		if (null != session) {
			SessionUser user = SessionUserUtil.getSessionUser(session);
			String actionKey = (String) uri.eval(scope);
			String resourceCode = (String) res.eval(scope);
			if (auth(user, actionKey, resourceCode)) {
				stat.exec(env, scope, writer);
			}
		}
	}

	public boolean hasEnd() {
		return true;
	}

	/**
	 * 判断用户是否有权限
	 * 
	 * @param user
	 *            --用户对象
	 * @param actionKey
	 *            --访问地址
	 * @param resourceCode
	 *            --资源编号
	 * @return
	 */
	private boolean auth(SessionUser user, String actionKey, String resourceCode) {
		if (user != null && StrKit.notBlank(actionKey)) {
		}
		return false;
	}
}