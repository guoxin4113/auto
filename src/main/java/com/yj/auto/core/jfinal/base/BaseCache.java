package com.yj.auto.core.jfinal.base;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.ehcache.CacheKit;
import com.yj.auto.Constants;

/**
 * service 缓存接口
 *
 */
public interface BaseCache {

	/**
	 * 获取缓存
	 * 
	 * @param key
	 * @return
	 */
	default <T> T getCache(Object key) {
		Object state = CacheKit.get(getCacheName(), Constants.CACHE_STATE_WAITTING);
		if (null != state) {// 缓存待加载状态
			CacheKit.remove(getCacheName(), Constants.CACHE_STATE_WAITTING);
			loadCache();
		}
		return CacheKit.get(getCacheName(), key);
	}

	/**
	 * 获取所有缓存，如果缓存的类型不一样时，有可能发生类型转换错误
	 * 
	 * @return
	 */
	default <T> List<T> getCaches() {
		List<T> list = new ArrayList<T>();
		List keys = CacheKit.getKeys(getCacheName());
		for (Object key : keys) {
			T e = getCache(key);
			if (null != e) {
				list.add(e);
			}
		}
		return list;
	}

	/**
	 * 添加缓存
	 * 
	 * @param key
	 * @param val
	 */
	default void addCache(Object key, Object val) {
		CacheKit.put(getCacheName(), key, val);
	}

	/**
	 * 清空缓存
	 */
	default void clearCache() {
		CacheKit.removeAll(getCacheName());
		addCache(Constants.CACHE_STATE_WAITTING, "true");
	}

	/**
	 * 组态名称
	 * 
	 * @return
	 */
	default String getCacheName() {
		return Constants.CACHE_NAME_DEFAULT;
	}

	/**
	 * 加载缓存
	 */
	public void loadCache();
}
