package com.yj.auto.core.web.log.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.log.model.bean.*;
/**
 * 阅读记录
 */
@SuppressWarnings("serial")
@Table(name = ReadingLog.TABLE_NAME, key = ReadingLog.TABLE_PK, remark = ReadingLog.TABLE_REMARK)
public class ReadingLog extends ReadingLogEntity<ReadingLog> {

}