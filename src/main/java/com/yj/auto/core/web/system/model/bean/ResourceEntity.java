package com.yj.auto.core.web.system.model.bean;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.yj.auto.core.jfinal.base.BaseEntity;

/**
 * 系统资源
 */
@SuppressWarnings("serial")
public abstract class ResourceEntity<M extends ResourceEntity<M>> extends BaseEntity<M> {

	public static final String TABLE_NAME = "t_sys_resource"; // 数据表名称

	public static final String TABLE_PK = "id"; // 数据表主键

	public static final String TABLE_REMARK = "系统资源"; // 数据表备注

	public String getTableName() {
		return TABLE_NAME;
	}

	public String getTableRemark() {
		return TABLE_REMARK;
	}

	public String getTablePK() {
		return TABLE_PK;
	}

	/**
	 * Column ：id
	 * 
	 * @return 资源主键
	 */

	public Integer getId() {
		return get("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * Column ：name
	 * 
	 * @return 名称
	 */
	@NotBlank
	@Length(max = 64)
	public String getName() {
		return get("name");
	}

	public void setName(String name) {
		set("name", name);
	}

	/**
	 * Column ：parent_id
	 * 
	 * @return 上级节点
	 */
	@NotNull
	public Integer getParentId() {
		return get("parent_id");
	}

	public void setParentId(Integer parentId) {
		set("parent_id", parentId);
	}

	/**
	 * Column ：url
	 * 
	 * @return 功能URL
	 */
	@Length(max = 128)
	public String getUri() {
		return get("uri");
	}

	public void setUri(String uri) {
		set("uri", uri);
	}

	/**
	 * Column ：img
	 * 
	 * @return 菜单图片
	 */
	@Length(max = 64)
	public String getIcon() {
		return get("icon");
	}

	public void setIcon(String icon) {
		set("icon", icon);
	}

	/**
	 * Column ：path
	 * 
	 * @return 资源路径
	 */
	@Length(max = 256)
	public String getPath() {
		return get("path");
	}

	public void setPath(String path) {
		set("path", path);
	}

	/**
	 * Column ：auth
	 * 
	 * @return 功能权限
	 */
	@Length(max = 512)
	public String getAuth() {
		return get("auth");
	}

	public void setAuth(String auth) {
		set("auth", auth);
	}

	/**
	 * Column ：target
	 * 
	 * @return 加载容器ID
	 */
	@Length(max = 32)
	public String getTarget() {
		return get("target");
	}

	public void setTarget(String target) {
		set("target", target);
	}

	/**
	 * Column ：state
	 * 
	 * @return 状态
	 */
	@NotBlank
	@Length(max = 32)
	public String getState() {
		return get("state");
	}

	public void setState(String state) {
		set("state", state);
	}

	/**
	 * Column ：sort
	 * 
	 * @return 排序
	 */
	@NotNull
	public Integer getSort() {
		return get("sort");
	}

	public void setSort(Integer sort) {
		set("sort", sort);
	}

	/**
	 * Column ：remark
	 * 
	 * @return 描述
	 */
	@Length(max = 1024)
	public String getRemark() {
		return get("remark");
	}

	public void setRemark(String remark) {
		set("remark", remark);
	}

	/**
	 * Column ：param1
	 * 
	 * @return 参数1
	 */
	@Length(max = 512)
	public String getParam1() {
		return get("param1");
	}

	public void setParam1(String param1) {
		set("param1", param1);
	}

	/**
	 * Column ：param2
	 * 
	 * @return 参数2
	 */
	@Length(max = 512)
	public String getParam2() {
		return get("param2");
	}

	public void setParam2(String param2) {
		set("param2", param2);
	}

	/**
	 * Column ：param3
	 * 
	 * @return 参数3
	 */
	@Length(max = 512)
	public String getParam3() {
		return get("param3");
	}

	public void setParam3(String param3) {
		set("param3", param3);
	}

	/**
	 * Column ：luser
	 * 
	 * @return 最后修改人
	 */

	public Integer getLuser() {
		return get("luser");
	}

	public void setLuser(Integer luser) {
		set("luser", luser);
	}

	/**
	 * Column ：ltime
	 * 
	 * @return 最后修改时间
	 */

	public Date getLtime() {
		return get("ltime");
	}

	public void setLtime(Date ltime) {
		set("ltime", ltime);
	}
}