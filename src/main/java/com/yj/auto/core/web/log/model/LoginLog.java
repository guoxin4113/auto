package com.yj.auto.core.web.log.model;

import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.web.log.model.bean.*;
/**
 * 登录日志
 */
@SuppressWarnings("serial")
@Table(name = LoginLog.TABLE_NAME, key = LoginLog.TABLE_PK, remark = LoginLog.TABLE_REMARK)
public class LoginLog extends LoginLogEntity<LoginLog> {

}