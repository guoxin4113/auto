package com.yj.auto.plugin.generator;

public class GeneratorTools {

	public static void main(String[] args) throws Exception {
		String tableRemovePrefixes = "t_com_";
		String viewPath = "component/base";
		String basepackage = "com.yj.auto";
		AutoGenerator gen = new AutoGenerator(tableRemovePrefixes, viewPath, basepackage);
		gen.generate("t_com_msg","t_com_msg_box");
		Runtime.getRuntime().exec("cmd.exe /c start " + gen.getTargetPath());
	}
}
