package com.yj.auto.plugin.log4j;

import java.io.Writer;

import org.apache.log4j.Layout;
import org.apache.log4j.WriterAppender;

/**
 * log4j输出处理类
 */
public class WebAppender extends WriterAppender {

	private Writer writer = new Log4jAsyncWriter();

	public WebAppender() {
		setWriter(writer);
	}

	public WebAppender(Layout layout) {
		this();
		super.layout = layout;
	}

}
