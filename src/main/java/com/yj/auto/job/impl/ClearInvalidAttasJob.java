package com.yj.auto.job.impl;

import org.quartz.JobExecutionContext;

import com.yj.auto.helper.AutoHelper;
import com.yj.auto.job.AutoJob;

// 清除无效附件
public class ClearInvalidAttasJob extends AutoJob {

	@Override
	public void run(JobExecutionContext context) {
		AutoHelper.getAttaService().deleteInvalidAttas();
	}

}
