### 登录日志列表
#sql("login.list")
	select * from t_log_login
	
	where 1 = 1
	#if(keyword)
		and ( user_code like #like(keyword) or user_name like #like(keyword) )
	#end
	
	#if(state)
		and logout_time is null
	#end
	
	#if(stime)
		and login_time >= #para(stime)
	#end	
	#if(etime)
		and login_time <= #para(etime)
	#end		
	
	#order()
	
#end

#sql("login.clear")
	delete from t_log_login
	where 1 = 1
	#if(keyword)
		and ( user_code like #like(keyword) or user_name like #like(keyword) )
	#end	
	#if(stime)
		and login_time >= #para(stime)
	#end	
	#if(etime)
		and login_time <= #para(etime)
	#end		
#end
### 访问日志列表
#sql("access.list")
	select * from t_log_access
	
	where 1 = 1
	#if(keyword)
		and ( user_code like #like(keyword) or user_name like #like(keyword) or uri like #like(keyword))
	#end

	#if(stime)
		and access_time >= #para(stime)
	#end	
	#if(etime)
		and access_time <= #para(etime)
	#end	
	
	#order()
#end

#sql("access.clear")
	delete from t_log_access
	
	where 1 = 1
	
	#if(keyword)
		and ( user_code like #like(keyword) or user_name like #like(keyword) or uri like #like(keyword))
	#end
	#if(stime)
		and access_time >= #para(stime)
	#end	
	#if(etime)
		and access_time <= #para(etime)
	#end	
#end

### 任务执行日志列表
#sql("schedule.list")
	select t_log_schedule.*,t_sys_schedule.code,t_sys_schedule.name from t_log_schedule left join t_sys_schedule on t_log_schedule.schedule_id=t_sys_schedule.id
	
	where 1 = 1

	#if(keyword)
		and ( t_sys_schedule.code like #like(keyword) or t_sys_schedule.name like #like(keyword) )
	#end
		
	#if(id!=null)
		and schedule_id = #para(id)
	#end
	#if(state)
		and t_log_schedule.state = #para(state)
	#end
	#if(stime)
		and stime >= #para(stime)
	#end	
	#if(etime)
		and stime <= #para(etime)
	#end	
	#order()
	
#end

#sql("schedule.clear")
	delete from t_log_schedule
	
	where 1 = 1
	
	#if(keyword)
		and exists ( select 1 from t_sys_schedule where  t_sys_schedule.id=t_log_schedule.schedule_id and ( t_sys_schedule.code like #like(keyword) or t_sys_schedule.name like #like(keyword)) )
	#end
		
	#if(id!=null)
		and schedule_id = #para(id)
	#end
	#if(state)
		and state = #para(state)
	#end
	#if(stime)
		and stime >= #para(stime)
	#end	
	#if(etime)
		and stime <= #para(etime)
	#end	
	
#end

### 阅读记录列表
#sql("reading.list")
	select t_log_reading.*,t_sys_user.code,t_sys_user.name from t_log_reading left join t_sys_user on t_log_reading.user_id=t_sys_user.id
	
	where 1 = 1
	#if(keyword)
		and ( t_sys_user.code like #like(keyword) or t_sys_user.name like #like(keyword) )
	#end

	#if(type)
		and type = #para(type)
	#end
		
	#if(stime)
		and read_time >= #para(stime)
	#end	
	#if(etime)
		and read_time <= #para(etime)
	#end		
	#order()
#end

#sql("reading.clear")
	delete from t_log_reading
	
	where 1 = 1
	#if(keyword)
		and exists ( select 1 from t_sys_user where  t_sys_user.id=t_log_reading.user_id and ( t_sys_user.code like #like(keyword) or t_sys_user.name like #like(keyword)) )
	#end
	#if(type)
		and type = #para(type)
	#end
	#if(stime)
		and read_time >= #para(stime)
	#end	
	#if(etime)
		and read_time <= #para(etime)
	#end		
#end

#sql("reading.delete")
	delete from t_log_reading where  type = #para(type) and  data_id in ( #for(id:idArray)#(for.index>0?",":"") #(id) #end )
#end