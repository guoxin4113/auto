package #(basepackage).controller;

import java.util.*;

import com.jfinal.kit.*;
import com.jfinal.log.Log;
import com.yj.auto.Constants;
import com.yj.auto.core.base.model.*;
import com.yj.auto.core.base.annotation.*;
import com.yj.auto.core.jfinal.base.*;
import com.yj.auto.plugin.table.model.DataTables;
import #(basepackage).model.*;
import #(basepackage).service.*;
/**
 * #(table.remarks) 管理	
 * 
 * 描述：
 * 
 */
@Controller(viewPath="#(viewPath)")
public class #(modelName)Controller extends BaseController {
	private static final Log logger = Log.getLog(#(modelName)Controller.class);
	
	private static final String RESOURCE_URI = "#(simpleName)/index";
	private static final String INDEX_PAGE ="#(simpleName)_index.html";
	private static final String FORM_PAGE = "#(simpleName)_form.html";
	private static final String SHOW_PAGE = "#(simpleName)_show.html";
	
	#(modelName)Service #(firstCharToLowerCase(modelName))Srv = null;
	
	public void index() {
		render(INDEX_PAGE);
	}
	
	public void list(QueryModel query) {
		DataTables<#(modelName)> dt = getDataTable(query, #(firstCharToLowerCase(modelName))Srv);
		renderJson(dt);
	}
	
	public void get() {
		Integer id = getParaToInt(0);
		#(modelName) model = #(firstCharToLowerCase(modelName))Srv.get(id);
		setAttr("model", model);
		render(SHOW_PAGE);
	}
	
	public void form() {
		Integer id= getParaToInt(0);
		#(modelName) model = null;
		if (null != id && 0 != id) {
			model = #(firstCharToLowerCase(modelName))Srv.get(id);
		} else {
			model = new #(modelName)();
		}
		setAttr("model", model);
		render(FORM_PAGE);
	}

	@Valid(type = #(modelName).class)
	public boolean saveOrUpdate(#(modelName) model) {
		model.setLuser(getSuId());
		model.setLtime(Constants.NOW());
		boolean success = false;
		if (null == model.getId()) {
			success = #(firstCharToLowerCase(modelName))Srv.save(model);
		} else {
			success = #(firstCharToLowerCase(modelName))Srv.update(model);
		}
		ResponseModel<String> res = renderSaveOrUpdate(success, #(firstCharToLowerCase(modelName))Srv);
		return res.isSuccess();
	}
	 
	public boolean delete() {
		ResponseModel<String> res = _delete(#(firstCharToLowerCase(modelName))Srv);
		return res.isSuccess();
	}
}
